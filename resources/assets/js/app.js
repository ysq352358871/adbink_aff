
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import moment from 'moment';

import ElementUI from "element-ui";
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'

Vue.use(ElementUI, { locale });

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('app-dashboard', require('./components/app/Dashboard.vue').default);

Vue.component('offer-list', require('./components/app/offers/OfferList').default);
Vue.component('offers-mine', require('./components/app/offers/OffersMine').default);

Vue.component('offer-show', require('./components/app/offers/OfferShow').default);

Vue.component('report', require('./components/app/report/Report').default);
Vue.component('billings', require('./components/app/billing/Billings').default);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('sl-user-list', require('./components/app/users/UserList.vue').default);

// Vuex
// import Vuex from 'vuex'
// const vuexStore = new Vuex.Store({
//     state: {
//         platform: 'app',
//         count: 0
//     },
//     mutations: {
//         increment (state) {
//             state.count++
//         }
//     }
// });
// window.vuexStore = vuexStore;
import ECharts from 'vue-echarts/components/ECharts'

// import ECharts modules manually to reduce bundle size
import 'echarts/lib/chart/bar'
import 'echarts/lib/component/tooltip'

// register component to use
Vue.component('chart', ECharts);

const app = new Vue({
    el: '#app',
    data: {
        msg: "hello",
    },
    computed: {},
    watch: {},
    events: {},
    created() {
        console.log('Bootstrap.');
        this.initLocale();
    },
    mounted() {
        console.log('Ready.');
    },
    methods: {
        initLocale() {
            console.log('Init Locale.');

            var that = this;
            var lang = this.locale;

            Vue.config.lang = lang;
            Vue.locale(lang, window.Someline.locales);

        },
    }
});
