@extends('app.layout.frame')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Offers</h1>
    </div>
    <div class="wrapper-md">
        <offer-list></offer-list>
    </div>

@endsection