@extends('app.layout.frame')

@section('content')
    <div class="bg-light lter b-b b-t wrapper-md d-flex align-center justify-space-between">
        <h1 class="m-n page-title h3 pull-left">
            {{ $offer->name }}
        </h1>
        <ul class="breadcrumb pull-right" style="margin-bottom: 0">
            <li><a href="/affiliate/offers">Offers</a></li>
            <li><a href="/affiliate/offers/{{ $offer->id }}">{{ $offer->name }}</a></li>
            <li style="color: #3da2df">{{ $offer->name }}</li>
        </ul>
    </div>
    <div class="wrapper-md">
        <offer-show :offer="{{ $offer }}"></offer-show>
    </div>
@endsection




