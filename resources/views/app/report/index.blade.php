@extends('app.layout.frame')

@section('content')

    <div class="bg-light lter b-b wrapper-md">
        <h1 class="m-n font-thin h3">Report</h1>
    </div>
    <div class="wrapper-md">
        <report></report>
    </div>

@endsection