<!-- nav -->
<nav ui-nav class="navi clearfix">
    <ul class="nav">
        <li class="hidden-folded padder m-t m-b-sm text-muted text-xs">
            <span>Navigation</span>
        </li>
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="fa fa-fw fa-dashboard icon {{ request()->url() == route('dashboard') ? 'text-primary-dker' : '' }}"></i>
                <span class="font-bold">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="javascript:;">
                  <span class="pull-right text-muted">
                    <i class="fa fa-fw fa-angle-right text"></i>
                    <i class="fa fa-fw fa-angle-down text-active"></i>
                  </span>
                <i class="glyphicon glyphicon-briefcase icon @if(request()->is('offers*')) text-primary-dker @endif"></i>
                <span class="font-bold">Offers</span>
            </a>
            <ul class="nav nav-sub dk">
                <li class="nav-sub-header">
                    <a href="javascript:;">
                        <span>Offers</span>
                    </a>
                </li>
                <li ui-sref-active="active">
                    <a href="{{ route('affiliate.offers') }}">
                        <span>Offers</span>
                    </a>
                </li>
                <li ui-sref-active="active">
                    <a href="{{ route('affiliate.offers.mine') }}">
                        <span>My offers</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="{{ route('affiliate.report') }}">
                <i class="glyphicon glyphicon-stats @if(request()->is('report*')) text-primary-dker @endif"></i>
                <span class="font-bold">Report</span>
            </a>
        </li>
{{--        <li>--}}
{{--            <a href="{{ route('billing') }}">--}}
{{--                <i class="fa fa-fw fa-book @if(request()->is('billing*')) text-primary-dker @endif"></i>--}}
{{--                <span class="font-bold">Billing</span>--}}
{{--            </a>--}}
{{--        </li>--}}
    </ul>
</nav>
<!-- nav -->