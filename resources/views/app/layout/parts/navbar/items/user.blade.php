<li class="dropdown">
    <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img src="{{ asset('assets/img/avatar-min.png') }}"
                     alt="...">
                  {{--<i class="on md b-white bottom"></i>--}}
              </span>
        <span class="">{{auth_user()->name}}</span> <b class="caret"></b>
    </a>
    <!-- dropdown -->
    <ul class="dropdown-menu animated fadeInRight w">
        <li>
            <a href="{{url('logout')}}"
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
                Logout
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
    <!-- / dropdown -->
</li>
