<?php

namespace Someline\Api\Controllers;

use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\CampaignCreateRequest;
use Someline\Http\Requests\CampaignUpdateRequest;
use Someline\Models\Campaign;
use Someline\Models\Relation;
use Someline\Repositories\Interfaces\CampaignRepository;
use Someline\Validators\CampaignValidator;

class CampaignsController extends BaseController
{

    /**
     * @var CampaignRepository
     */
    protected $repository;

    /**
     * @var CampaignValidator
     */
    protected $validator;

    public function __construct(CampaignRepository $repository, CampaignValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->repository->paginate();
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CampaignCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $campaign = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $campaign;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CampaignUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(CampaignUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $campaign = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function getOffers(Request $request)
    {
        $id = $request->get('c_id');
        $search = $request->get('search');

        $perPage = $request->get('paginationPerPage',20);

        $geo =  $request->get('geo');

        $data = Campaign::query()
            ->leftJoin('_tb_apply',function($q) use ($id){
                $q->on('_tb_campaign.id','=','_tb_apply.campaign_id')
                    ->where('_tb_apply.customer_id',$id);
            })
            ->leftJoin('_tb_relation',function ($q) use ($id){
                $q->on('_tb_campaign.id','=','_tb_relation.campaign')
                    ->where('_tb_relation.customer',$id);
            })
            ->select(
                'name','_tb_campaign.id as id','geo','package_name','os','_tb_campaign.price as price',
                '_tb_campaign.daily as daily','_tb_campaign.live as live',
                '_tb_campaign.status as status','_tb_campaign.public as public'
            )
            ->when($geo, function ($sub_query) use ($geo) {
                return $sub_query->where('geo', 'like', '%' . $geo . '%');
            })
            ->when($search, function ($sub_query) use ($search) {
                return $sub_query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('package_name', 'like', '%' . $search . '%');
            })
            ->where([
                ['_tb_relation.customer','=',null],
                ['_tb_apply.id','=',null]
            ])
            ->where('_tb_campaign.live','=',1)
            ->orderBy('id', 'desc')->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    public function canGetTrackingLink(Request $request)
    {
        $aff_id = $request->get('aff_id');
        $offer_id = $request->get('offer_id');

        $results = Relation::where([
            ['campaign','=',$offer_id],
            ['customer','=',$aff_id]
        ])->get();
        return count($results);
    }
}
