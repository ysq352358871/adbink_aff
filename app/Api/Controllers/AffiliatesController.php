<?php

namespace Someline\Api\Controllers;

use Carbon\Carbon;
use Carbon\CarbonInterval;
use Dingo\Api\Exception\DeleteResourceFailedException;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Someline\Http\Requests\AffiliateCreateRequest;
use Someline\Http\Requests\AffiliateUpdateRequest;
use Someline\Models\Affiliate;
use Someline\Models\Campaign;
use Someline\Models\Summarry;
use Someline\Repositories\Interfaces\AffiliateRepository;
use Someline\Validators\AffiliateValidator;

class AffiliatesController extends BaseController
{

    /**
     * @var AffiliateRepository
     */
    protected $repository;

    /**
     * @var AffiliateValidator
     */
    protected $validator;

    public function __construct(AffiliateRepository $repository, AffiliateValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->repository->paginate();
    }

    /**
     * Display all resources.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        return $this->repository->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AffiliateCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AffiliateCreateRequest $request)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $affiliate = $this->repository->create($data);

        // throw exception if store failed
//        throw new StoreResourceFailedException('Failed to store.');

        // A. return 201 created
//        return $this->response->created(null);

        // B. return data
        return $affiliate;

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AffiliateUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(AffiliateUpdateRequest $request, $id)
    {

        $data = $request->all();

        $this->validator->with($data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $affiliate = $this->repository->update($data, $id);

        // throw exception if update failed
//        throw new UpdateResourceFailedException('Failed to update.');

        // Updated, return 204 No Content
        return $this->response->noContent();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if ($deleted) {
            // Deleted, return 204 No Content
            return $this->response->noContent();
        } else {
            // Failed, throw exception
            throw new DeleteResourceFailedException('Failed to delete.');
        }
    }

    public function getRelationOffers(AffiliateUpdateRequest $request,$id)
    {
        $perPage = (int)$request->get('paginationPerPage',20);

        $is_aff = (boolean)$request->get('is_aff',false); // 是否从渠道端获取渠道的offer

        $search = $request->get('search','');

        $query = Campaign::query()
            ->join('_tb_relation as r',function ($q){
                return $q->on('r.campaign','=','_tb_campaign.id');
            });
        $select = array(
            '_tb_campaign.id as id',
            '_tb_campaign.name as name'
        );
        $campaigns = $query->where('r.customer',$id)
            ->select($select)
            ->when($search,function ($q) use ($search){
                $q->where('name', 'like', '%' . $search . '%');
            })
            ->get();


        // 手动分页
        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $campaigns->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $campaigns->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return response()->json([
            'data' => $paginatedResult,
        ]);
    }

    public function getCustomerOffers(Request $request,$id)
    {
        $search = $request->get('search');

        $perPage = $request->get('paginationPerPage',20);

        $geo =  $request->get('geo');

        $data = Campaign::with([
            'applies'=>function($q) use ($id){
                $q->where('customer_id',$id)
                    ->select('id','campaign_id','status','updated_at','customer_id');
            }
        ])
            ->leftJoin('_tb_relation',function ($q) use ($id){
                $q->on('_tb_campaign.id','=','_tb_relation.campaign')
                    ->where('_tb_relation.customer',$id);
            })
            ->select(
                'name','_tb_campaign.id as id','geo','package_name','os','_tb_campaign.price as price',
                '_tb_campaign.daily as daily','_tb_campaign.live as live',
                '_tb_campaign.status as status'
            )
            ->where(function ($query) use ($id){
                $query->where('_tb_relation.customer',$id)
                    ->orWhereHas('applies', function($query) use ($id){
                        $query->where('customer_id', $id);

                    });
            })
            ->when($geo, function ($sub_query) use ($geo) {
                return $sub_query->where('geo', 'like', '%' . $geo . '%');
            })
            ->when($search, function ($sub_query) use ($search) {
                return $sub_query->where('name', 'like', '%' . $search . '%')
                    ->orWhere('package_name', 'like', '%' . $search . '%');
            })
            ->orderBy('id', 'desc')->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }

    public function getCustomerTops(Request $request,$id)
    {
        $top = $request->get('top',20);

        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $query = Campaign::query();


        $data = $query->leftJoin('_tb_summarry AS s','_tb_campaign.id','=','s.campaign')
            ->whereBetween(\DB::raw("date_format(s.date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)])
            ->where('s.customer',$id)
            ->select(
                '_tb_campaign.id as id',
                '_tb_campaign.name as name',
                \DB::raw('SUM(s.payment) as revenue')
            )
            ->groupBy('id')
            ->orderBy('revenue','desc')
            ->take($top)
            ->get();


        return response()->json([
            'data' => $data
        ]);
    }

    public function getCustomerGraph(Request $request,$id)
    {
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $results = Summarry::whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"), [new Carbon($start_date),new Carbon($end_date)])
            ->where('customer',$id)
            ->groupBy('date')
            ->orderBy('date')
            ->get([
                \DB::raw('DATE_FORMAT(date, "%Y-%m-%d") as date'),
                \DB::raw('SUM(callback) as callback'),
                \DB::raw('SUM(tracking) as tracking'),
                \DB::raw('SUM(payment) as profit')
            ])
            ->keyBy('date')
            ->map(function ($item) {
                $item->date = Carbon::parse($item->date)->format('Y-m-d');
                return $item;
            });
        $period = new \DatePeriod(new Carbon($start_date), CarbonInterval::day(), (new Carbon($end_date))->addDay());

        $data = array_map(function ($datePeriod) use ($results) {
            $date = $datePeriod->format('Y-m-d');
            if($results->has($date)){
                return $results->get($date);
            }
            return array(
                'date'=>$date,
                'callback'=>0,
                'tracking'=>0,
                'profit'=>0
            );

        }, iterator_to_array($period));

        return response()->json([
            'data' => $data
        ]);
    }


    public function getCustomerReport(Request $request,$id)
    {
        $perPage = $request->get('paginationPerPage',20);
        $offers = $request->get('offers');
        $package_name = $request->get('package_name');
        $start_date = $request->get('filterRange')[0];
        $end_date = $request->get('filterRange')[1];

        $group = array( 'campaign','customer');

        $getFiled=array(
            \DB::raw('SUM(payment) as payment'),
            \DB::raw('SUM(tracking) as tracking'),
            \DB::raw('SUM(callback) as callback'),
            'campaign',
        );
        $query = Summarry::with([
            'campaign' => function($query){
                $query->select('id','name','package_name');
            }
        ]);
        $query->where('customer',$id)
            ->when($offers, function ($query) use ($offers) {
                $query->whereHas('campaign', function ($sub_query) use ($offers) {
                    $sub_query->whereIn('id',$offers);
                });
            })
            ->when($package_name, function ($query) use ($package_name) {
                $query->whereHas('campaign', function ($sub_query) use ($package_name) {
                    $sub_query->where('package_name', 'like', '%' . $package_name . '%');
                });
            })
            ->whereBetween(\DB::raw("date_format(date,'%Y-%m-%d %H:%i:%s')"),[new Carbon($start_date),new Carbon($end_date)]);
        $data = $query->groupBy($group)
            ->orderBy('date','desc')
            ->select($getFiled)->get();

        $currentPage = LengthAwarePaginator::resolveCurrentPage() - 1;

        $currentPageResult = $data->slice($currentPage * $perPage, $perPage)->values();
        $paginatedResult = new LengthAwarePaginator($currentPageResult, $data->count(), $perPage);
        $paginatedResult->setPath(\request()->url());

        return $paginatedResult;
    }
}
