<?php

namespace Someline\Api\Controllers;

use Illuminate\Http\Request;

use Someline\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Someline\Http\Requests\RelationCreateRequest;
use Someline\Http\Requests\RelationUpdateRequest;
use Someline\Repositories\Interfaces\RelationRepository;
use Someline\Validators\RelationValidator;

/**
 * Class RelationsController.
 *
 * @package namespace Someline\Api\Controllers;
 */
class RelationsController extends Controller
{
    /**
     * @var RelationRepository
     */
    protected $repository;

    /**
     * @var RelationValidator
     */
    protected $validator;

    /**
     * RelationsController constructor.
     *
     * @param RelationRepository $repository
     * @param RelationValidator $validator
     */
    public function __construct(RelationRepository $repository, RelationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $relations = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $relations,
            ]);
        }

        return view('relations.index', compact('relations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RelationCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(RelationCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $relation = $this->repository->create($request->all());

            $response = [
                'message' => 'Relation created.',
                'data'    => $relation->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relation = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $relation,
            ]);
        }

        return view('relations.show', compact('relation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $relation = $this->repository->find($id);

        return view('relations.edit', compact('relation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RelationUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(RelationUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $relation = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Relation updated.',
                'data'    => $relation->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Relation deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Relation deleted.');
    }
}
