<?php

namespace Someline\Http\Controllers\Auth;

use Someline\Http\Controllers\BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends BaseController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('angulr.auth.login');
    }

    public function postLogin()
    {
        $this->validate(request(),[
            'email'=>'required|email',
            'password'=>'required',
        ],[
            'email.required'=>'Email is required',
            'email.email'=>'The email address entered is invalid',
            'password.required'=>'Password is required',
        ]);

        $user = request(['email','password']);
//        $is_remember = boolval(request('is_remember'));
        if (\Auth::attempt($user))
        {
            return redirect('/');
        }
        return redirect()->back()->withErrors('E-mail or password input error');

    }

}
