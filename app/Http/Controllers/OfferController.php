<?php


namespace Someline\Http\Controllers;


use Someline\Http\Controllers\BaseController;
use Someline\Models\Campaign;
use Someline\Models\Country;

class OfferController extends BaseController
{

    public function Index()
    {
        return view('app.offers.index');
    }

    public function show($id)
    {
        $offer = Campaign::find($id);

        $offer->geo = Country::whereIn('code',explode(',',$offer->geo))->pluck('country');

        return view('app.offers.show',[
            'offer'=>$offer
        ]);
    }
}