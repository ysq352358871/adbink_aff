<?php

namespace Someline\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\RelationRepository;
use Someline\Models\Relation;
use Someline\Validators\RelationValidator;

/**
 * Class RelationRepositoryEloquent.
 *
 * @package namespace Someline\Repositories\Eloquent;
 */
class RelationRepositoryEloquent extends BaseRepository implements RelationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Relation::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RelationValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
