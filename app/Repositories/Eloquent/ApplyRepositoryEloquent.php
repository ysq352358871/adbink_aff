<?php

namespace Someline\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\ApplyRepository;
use Someline\Models\Apply;
use Someline\Validators\ApplyValidator;

/**
 * Class ApplyRepositoryEloquent.
 *
 * @package namespace Someline\Repositories\Eloquent;
 */
class ApplyRepositoryEloquent extends BaseRepository implements ApplyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Apply::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ApplyValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
