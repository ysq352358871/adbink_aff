<?php

namespace Someline\Repositories\Eloquent;

use Someline\Repositories\Criteria\RequestCriteria;
use Someline\Repositories\Interfaces\AffiliateRepository;
use Someline\Models\Affiliate;
use Someline\Validators\AffiliateValidator;
use Someline\Presenters\AffiliatePresenter;

/**
 * Class AffiliateRepositoryEloquent
 * @package namespace Someline\Repositories\Eloquent;
 */
class AffiliateRepositoryEloquent extends BaseRepository implements AffiliateRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Affiliate::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AffiliateValidator::class;
    }


    /**
    * Specify Presenter class name
    *
    * @return mixed
    */
    public function presenter()
    {

        return AffiliatePresenter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
