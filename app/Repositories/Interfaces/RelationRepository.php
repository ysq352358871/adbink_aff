<?php

namespace Someline\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RelationRepository.
 *
 * @package namespace Someline\Repositories\Interfaces;
 */
interface RelationRepository extends RepositoryInterface
{
    //
}
