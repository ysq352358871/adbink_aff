<?php

namespace Someline\Repositories\Interfaces;

use Someline\Repositories\Interfaces\BaseRepositoryInterface;

/**
 * Interface AffiliateRepository
 * @package namespace Someline\Repositories\Interfaces;
 */
interface AffiliateRepository extends BaseRepositoryInterface
{
    //
}
