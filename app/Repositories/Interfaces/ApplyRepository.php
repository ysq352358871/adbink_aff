<?php

namespace Someline\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ApplyRepository.
 *
 * @package namespace Someline\Repositories\Interfaces;
 */
interface ApplyRepository extends RepositoryInterface
{
    //
}
