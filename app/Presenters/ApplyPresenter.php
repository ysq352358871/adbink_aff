<?php

namespace Someline\Presenters;

use Someline\Transformers\ApplyTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ApplyPresenter.
 *
 * @package namespace Someline\Presenters;
 */
class ApplyPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ApplyTransformer();
    }
}
