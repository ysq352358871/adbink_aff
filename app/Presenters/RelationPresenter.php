<?php

namespace Someline\Presenters;

use Someline\Transformers\RelationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RelationPresenter.
 *
 * @package namespace Someline\Presenters;
 */
class RelationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RelationTransformer();
    }
}
