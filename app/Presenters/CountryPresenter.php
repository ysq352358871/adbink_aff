<?php

namespace Someline\Presenters;

use Someline\Transformers\CountryTransformer;

/**
 * Class CountryPresenter
 *
 * @package namespace Someline\Presenters;
 */
class CountryPresenter extends BasePresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CountryTransformer();
    }
}
