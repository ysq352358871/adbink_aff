<?php

namespace Someline\Transformers;

use Someline\Models\Relation;

/**
 * Class RelationTransformer
 * @package namespace Someline\Transformers;
 */
class RelationTransformer extends BaseTransformer
{

    /**
     * Transform the Relation entity
     * @param Relation $model
     *
     * @return array
     */
    public function transform(Relation $model)
    {
        return [
            'id' => (int) $model->id,
            'margin'=> $model->margin,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
