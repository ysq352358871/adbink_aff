<?php

namespace Someline\Transformers;

use Someline\Base\Models\BaseModel;
use Someline\Models\Campaign;

/**
 * Class CampaignTransformer
 * @package namespace Someline\Transformers;
 */
class CampaignTransformer extends BaseTransformer
{
    protected $availableIncludes = [
        'billing','jump','events','applies'
    ];
    /**
     * Transform the Campaign entity
     * @param Campaign $model
     *
     * @return array
     */
    public function transform(Campaign $model)
    {
        return [
            'id' => (int) $model->id,

            /* place your other model properties here */
            'name' => $model->name,
            'link' => $model->link,
            'geo' => $model->geo,
            'os' => $model->os,
            'price' => $model->price,
            'daily' => $model->daily,
            'margin' => $model->margin,
            'advertiser'=> $model->advertiser,
            'mark'=> $model->mark,
            'live'=> (int)$model->live,
            'status'=> (int)$model->status,
            'anti_cheating'=> (int)$model->anti_cheating,
            'payout'=> $model->payout,
            'paytype'=> $model->paytype,
            'preview_url' => $model->preview_url,
            'adv_oid' => $model->adv_oid,
            'package_name' => $model->package_name,
            'icon' => $model->icon,
            'start_time' => $model->start_time,
            'end_time' => $model->end_time,
            'created_at' => (string) $model->created_at,
//            'pivot'=>$model->pivot
//            'updated_at' => (string) $model->updated_at
        ];
    }

    /**
     * Include Billing---->advertiser
     * @param BaseModel $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeBilling(BaseModel $model)
    {
        $advertiser = $model->billing; //advertiser
        if ($advertiser) {
            return $this->item($advertiser, new AdvertiserTransformer());
        }
        return null;
    }

    /**
     * Include Jump
     * @param BaseModel $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeJump(BaseModel $model)
    {
        $jump = $model->jump;
        if ($jump) {
            return $this->item($jump, new JumpTransformer());
        }
        return null;
    }


    public function includeEvents(BaseModel $model)
    {
        $events = $model->events;
        if ($events) {
            return $this->collection($events, new EventTransformer());
        }
        return [];
    }
    public function includeApplies(BaseModel $model)
    {
        $applies = $model->applies;
        if ($applies) {
            return $this->collection($applies, new ApplyTransformer());
        }
        return [];
    }
}
