<?php

namespace Someline\Transformers;

use League\Fractal\TransformerAbstract;
use Someline\Models\Apply;

/**
 * Class ApplyTransformer.
 *
 * @package namespace Someline\Transformers;
 */
class ApplyTransformer extends TransformerAbstract
{
    /**
     * Transform the Apply entity.
     *
     * @param \Someline\Models\Apply $model
     *
     * @return array
     */
    public function transform(Apply $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
