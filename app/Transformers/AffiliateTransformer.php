<?php

namespace Someline\Transformers;

use Someline\Base\Models\BaseModel;
use Someline\Models\Affiliate;

/**
 * Class AffiliateTransformer
 * @package namespace Someline\Transformers;
 */
class AffiliateTransformer extends BaseTransformer
{
    protected $availableIncludes = [
        'campaigns'
    ];

    /**
     * Transform the Affiliate entity
     * @param Affiliate $model
     *
     * @return array
     */
    public function transform(Affiliate $model)
    {
        return [
            'id' => (int) $model->id,
            'user_id' => (int) $model->user_id,
            'name' => $model->name,
            'email' => $model->email,
            'platform' => $model->platform,
            'postback' => $model->postback,
            'mark' => $model->mark,
            'live' => $model->live,
            'company_name' => $model->company_name,
            'contact_name' => $model->contact_name,
            'im' => $model->im,
            'discount' => $model->discount,
            'margin' => $model->margin,
            'tag' => $model->tag,
            'api_key' => $model->api_key,
            'exclude' => $model->exclude,
            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }

    /**
     * Include Campaigns
     * @param BaseModel $model
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCampaigns(BaseModel $model)
    {
        $campaigns = $model->campaigns;
        if ($campaigns) {
            return $this->collection($campaigns, new CampaignTransformer());
        }
        return [];
    }

}
