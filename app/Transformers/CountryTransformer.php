<?php

namespace Someline\Transformers;

use Someline\Models\Country;

/**
 * Class CountryTransformer
 * @package namespace Someline\Transformers;
 */
class CountryTransformer extends BaseTransformer
{

    /**
     * Transform the Country entity
     * @param Country $model
     *
     * @return array
     */
    public function transform(Country $model)
    {
        return [
            'code' => (string) $model->code,
            'country' => (string) $model->country,
            'payout' => (string) $model->payout,

            /* place your other model properties here */

            'created_at' => (string) $model->created_at,
            'updated_at' => (string) $model->updated_at
        ];
    }
}
