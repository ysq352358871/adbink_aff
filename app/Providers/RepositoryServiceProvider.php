<?php

namespace Someline\Providers;

use Illuminate\Support\ServiceProvider;
use Someline\Repositories\Eloquent\UserRepositoryEloquent;
use Someline\Repositories\Interfaces\UserRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\CampaignRepository::class, \Someline\Repositories\Eloquent\CampaignRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\AffiliateRepository::class, \Someline\Repositories\Eloquent\AffiliateRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\CountryRepository::class, \Someline\Repositories\Eloquent\CountryRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\RelationRepository::class, \Someline\Repositories\Eloquent\RelationRepositoryEloquent::class);
        $this->app->bind(\Someline\Repositories\Interfaces\ApplyRepository::class, \Someline\Repositories\Eloquent\ApplyRepositoryEloquent::class);
        //:end-bindings:
    }
}
