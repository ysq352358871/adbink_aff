<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Relation extends BaseModel implements Transformable
{
    use TransformableTrait;

//    protected $primaryKey = 'id';

    protected $fillable = ['live','daily','price','margin'];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];
    protected  $table='_tb_relation';

    /**
     * 用来向表中插入数据的字段
     *
     * @var array
     */
    protected $tableColumns = [
        'campaign',
        'customer',
        'price',
        'daily',
        'margin',
        'live',
        'created_at',
        'updated_at'
    ];

}
