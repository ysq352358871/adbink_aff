<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Campaign extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

//    protected $fillable = ['id'];
    protected $guarded = ['id'];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_campaign';

    public function affiliates()
    {
        return $this->belongsToMany(Affiliate::class,'_tb_relation','campaign','customer')->withPivot('id','price', 'daily','live','margin','created_at');
    }

    public function billing()
    {
        return $this->belongsTo(Advertiser::class, 'advertiser');
    }

    public function jump()
    {
        return $this->hasOne(Jump::class,'campaign_id');
    }

    public function events()
    {
        return $this->hasMany(Event::class,'campaign_id');
    }
    public function applies()
    {
        return $this->hasMany(Apply::class,'campaign_id');
    }

    public function summarries()
    {
        return $this->hasMany(Summarry::class,'campaign');
    }


    public function scopeWhereContentContain($query, $search)
    {
        return $query->where('id', 'like', '%' . $search . '%')
            ->orWhere('name', 'like', '%' . $search . '%')
            ->orWhere('link', 'like', '%' . $search . '%')
            ->orWhere('geo', 'like', '%' . $search . '%')
            ->orWhere('os', 'like', '%' . $search . '%');
    }

}
