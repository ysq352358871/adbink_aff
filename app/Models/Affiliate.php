<?php

namespace Someline\Models;

use Illuminate\Notifications\Notifiable;
use Someline\Models\BaseModel;
//use Prettus\Repository\Contracts\Transformable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Someline\Notifications\User\ResetPasswordNotification;

class Affiliate extends Authenticatable
{
    use TransformableTrait,Notifiable;

    protected $primaryKey = 'id';

    protected $fillable = [];

    protected $guarded = ['id'];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected  $table='_tb_customer';

    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class,'_tb_relation','customer','campaign')->withPivot('id','price', 'daily','live','margin','created_at');
    }

    public function scopeWhereContentContain($query, $search)
    {
        return $query->where('id', 'like', '%' . $search . '%')
            ->orWhere('name', 'like', '%' . $search . '%')
            ->orWhere('email', 'like', '%' . $search . '%')
            ->orWhere('company_name', 'like', '%' . $search . '%')
            ->orWhere('im', 'like', '%' . $search . '%')
            ->orWhere('contact_name', 'like', '%' . $search . '%')
            ->orWhere('postback', 'like', '%' . $search . '%');
    }

//    public function sendPasswordResetNotification($token)
//    {
//        $this->notify(new ResetPasswordNotification($token));
//    }

    public function routeNotificationForMail()
    {
        return $this->email;
    }

}
