<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Summarry extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

    protected $fillable = [];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_summarry';

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign');
    }

    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'customer');
    }

    public function scopeAdvertiser($query, $advertiser_id)
    {
        return $query->whereHas('campaign.billing', function ($sub_query) use ($advertiser_id) {
            $sub_query->where('id', $advertiser_id);
        });
    }

}
