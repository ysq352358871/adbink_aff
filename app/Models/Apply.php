<?php

namespace Someline\Models;

use Someline\Models\BaseModel;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Apply extends BaseModel implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

//    protected $fillable = [];
    protected $guarded = ['id'];

    // Fields to be converted to Carbon object automatically
    protected $dates = [];

    protected  $table='_tb_apply';

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function affiliate()
    {
        return $this->belongsTo(Affiliate::class, 'customer_id');
    }
}
