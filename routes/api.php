<?php

use Illuminate\Http\Request;
use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// v1
$api->version('v1', [
    'namespace' => 'Someline\Api\Controllers',
    'middleware' => ['api']
], function (Router $api) {

    $api->group(['middleware' => ['auth:api']], function (Router $api) {

        // Rate: 100 requests per 5 minutes
        $api->group(['middleware' => ['api.throttle'], 'limit' => 100, 'expires' => 5], function (Router $api) {

            // customer
            $api->group(['prefix' => 'customers'], function (Router $api) {
                $api->get('/offers', 'CampaignsController@getOffers');
                $api->get('/{id}/offers/search', 'AffiliatesController@getRelationOffers');
                $api->get('/{id}/offers', 'AffiliatesController@getCustomerOffers');
                $api->get('/{id}/offer/top', 'AffiliatesController@getCustomerTops');
                $api->get('/{id}/graph', 'AffiliatesController@getCustomerGraph');
                $api->get('/{id}/report', 'AffiliatesController@getCustomerReport');
            });

            // /country
            $api->group(['prefix' => 'countries'], function (Router $api) {
                $api->get('/', 'CountriesController@index');
                $api->get('/all', 'CountriesController@all');
            });

            //apply
            $api->post('/apply', 'AppliesController@apply');

            //offer
            $api->get('/check','CampaignsController@canGetTrackingLink');




            // /users
            $api->group(['prefix' => 'users'], function (Router $api) {
                $api->get('/', 'UsersController@index');
                $api->post('/', 'UsersController@store');
                $api->get('/me', 'UsersController@me');
                $api->get('/{id}', 'UsersController@show');
                $api->put('/{id}', 'UsersController@update');
                $api->delete('/{id}', 'UsersController@destroy');
            });

        });

    });

});