<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth Routes
Auth::routes();

// Basic Routes
//Route::get('/home', 'HomeController@index');

Route::post('login', 'Auth\LoginController@postLogin');
Route::post('register', 'Auth\RegisterController@register');



// Protected Routes
Route::group(['middleware' => 'auth'], function () {

    Route::view('/', 'app.index')->name('dashboard');
    Route::get('/offers', 'OfferController@Index')->name('affiliate.offers');
    Route::view('/offers/mine', 'app.offers.mine')->name('affiliate.offers.mine');
    Route::get('/offers/{id}','OfferController@show')->name('affiliate.offers.show');
    Route::view('/report','app.report.index')->name('affiliate.report');
    Route::view('/billing','app.billing.billing')->name('billing');

//    Route::get('/', 'ExampleController@getIndexExample');
    Route::get('blank-example', 'ExampleController@getBlankExample');
    Route::get('desktop-example', 'ExampleController@getDesktopExample');

    Route::get('users', 'UserController@getUserList');

});

// Image Routes
// @WARNING: The 'image' prefix is reserved for SomelineImageService
Route::group(['prefix' => 'image'], function () {

    Route::group(['middleware' => 'auth'], function () {
        Route::post('/', 'ImageController@postImage');
    });

    Route::get('{type}/{name}', 'ImageController@showTypeImage');
    Route::get('/{name}', 'ImageController@showOriginalImage');

});

// Locale Routes
// @WARNING: The 'locales' prefix is reserved for SomelineLocaleController
Route::group(['prefix' => 'locales'], function () {

    Route::get('/{locale}.js', '\Someline\Support\Controllers\LocaleController@getLocaleJs');

    Route::get('/switch/{locale}', '\Someline\Support\Controllers\LocaleController@getSwitchLocale');

});